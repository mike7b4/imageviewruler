import QtQuick 2

Rectangle {
    anchors.fill: parent
    color: "cyan"
    ListView {
        id: view
        model: images
        anchors.fill:parent
        delegate: MouseArea {
            width: parent.width
            height: window.height/2
            Image {
                anchors.fill: parent
                cache: false
                asynchronous: true
                fillMode: Image.PreserveAspectFit
                source: "file://"+model.filename
            }
            onClicked: {
                images.set_current_index(view.currentIndex)
                stackView.replace(image)
            }
        }
    }
}
