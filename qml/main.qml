import QtQuick 2.10
import QtQuick.Controls 2.3
import ImageModel 1.0
ApplicationWindow {
    id: window
    visible: true
    width: 1040
    height: 480
    title: qsTr("Stack")
//    property string current: -1

    ImageModel {
        id: images
        property int currentIndex: -1

        Component.onCompleted: {
            console.log("LOADED")
            set_path("")
        }
        function set_current_index(index) {
//           if (images.currentIndex >= images.length())
 //               images.currentIndex = -1;
  //         } else {
               images.currentIndex = index;
                image.file = images.get(images.currentIndex)
   //        }
        }
    }

    ImageModel {
        id: directories
        directory: true

        Component.onCompleted: {
            console.log("LOADED")
            set_path("")
        }
    }


    header: ToolBar {
        contentHeight: 40

        ComboBox {
            anchors.left: parent.left
            anchors.right: parent.right
            model: directories
            textRole: "filename"
            onCurrentIndexChanged: {
                //console.log(directories.get(currentIndex))
                images.set_path(directories.get(currentIndex))
            }
        }
    }

    MouseArea {
        id: image
        property string file: ""
        anchors.fill: parent
        Rectangle {
            anchors.fill: parent
            color: "blue"
            Image {
                anchors.fill: parent
                source: "file:///"+image.file
                fillMode: Image.PreserveAspectFit
                cache: false
            }
        }
        onClicked: {
            images.set_current_index(images.currentIndex+1);
        }
        onPressAndHold: {
            stackView.replace(thumbnails)
        }
    }

    StackView {
        id: stackView
        initialItem: Thumbnails { id: thumbnails }
        anchors.fill: parent
    }

}
