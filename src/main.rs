use std::collections::HashMap;
use std::fs::File;
use std::ffi::CStr;
use std::path::PathBuf;
use std::fs;
use std::io::Read;
use qmetaobject::*;
use serde_derive::Deserialize;
use toml;
use dirs;
#[derive(Default)]
struct ImageInfo {
    filename: String,
}
#[derive(Deserialize, Debug)]
struct Config {
    path: PathBuf,
}

#[derive(QObject, Default)]
struct ImageModel {
    base: qt_base_class!( trait QAbstractListModel ),
    count: qt_property!(i32; READ row_count NOTIFY count_changed),
    directory: qt_property!(bool; NOTIFY directory_changed),
    directory_changed: qt_signal!(),
    count_changed: qt_signal!(),
    list: Vec<ImageInfo>,
    get: qt_method!(fn(&self, index: i32) -> String),
    set_path: qt_method!(fn(&mut self, path: String)),
    clear: qt_method!(fn(&mut self)),
}

impl ImageModel {
    fn update(&mut self, path: &PathBuf) -> Result<(), std::io::Error> {
        self.clear();
        let mut count = 0;
        let mut end = self.list.len();
        let mut paths: Vec<_> = fs::read_dir(&path).unwrap()
            .map(|r| r.unwrap())
            .collect();
        paths.sort_by_key(|dir| dir.path());

        for entry in paths {
            //let entry = entry?;
            if (entry.file_type()?.is_dir() && self.directory) || 
                (self.directory == false && entry.file_type()?.is_dir() == false) {
                (self as &mut QAbstractListModel).begin_insert_rows(end as i32, end as i32);
                count += 1;
                let mut item = ImageInfo::default();
                item.filename = String::from(entry.path().into_os_string().into_string().unwrap());
                self.list.insert(end, item);
                end += 1;
 
                (self as &mut QAbstractListModel).end_insert_rows();
            }
        }
        println!("PUSHED: {} also {}", count, self.list.len());
        self.count_changed();
        Ok(())
    }

    // Called from QML we only allow pictures for now
    fn set_path(&mut self, inpath: String) {
        let mut path = PathBuf::from(inpath);
        if path.has_root() {
            path = match self.load_config() {
                Ok(config) => {
                    config.path
                },
                Err(_) => {
                    dirs::picture_dir().unwrap()
                }
            };
        }

        // TODO error
        self.update(&path).expect(&format!("path fail {:?}", &path));
    }

    fn on_error(&mut self, message: String) {
        eprintln!("{}", message);
    }

    fn load_config(&self) -> Result<Config, std::io::Error> {
        let mut confdir = PathBuf::from(dirs::config_dir().unwrap());
        confdir.push("imageviewruler/config.toml");
        let mut file = File::open(confdir)?;
        let mut contents = String::new();
        file.read_to_string(&mut contents)?;
        match toml::from_str(&contents) {
            Ok(config) => {
                Ok(config)
            },
            Err(_) => {
                Err(std::io::Error::new(std::io::ErrorKind::InvalidInput, "parse error"))
            }
        }
    }
    fn get(&self, idx: i32) -> String {
        if (idx as usize) < self.list.len() {
            return self.list.get(idx as usize).unwrap().filename.to_string();
        }
        "".to_string()
    }

    fn clear(&mut self) {
        let len = self.list.len();
        (self as &mut QAbstractListModel).begin_remove_rows(0 as i32, len as i32);
        self.list.clear();
        (self as &mut QAbstractListModel).end_remove_rows();

        self.count_changed();
    }
}

impl QAbstractListModel for ImageModel {
    fn row_count(&self) -> i32 {
        self.list.len() as i32
    }

///    fn set_directory(&mut self, dir: bool) {
 //       self.directory = dir;
  //  }

    fn data(&self, index: QModelIndex, role:i32) -> QVariant {
        let idx = index.row() as usize;
        if idx < self.list.len() {
            if role == USER_ROLE { QString::from(self.list[idx].filename.clone()).into() }
            else { QVariant::default() }
        } else {
            QVariant::default()
        }
    }
    fn role_names(&self) -> HashMap<i32, QByteArray> {
        let mut map = HashMap::new();
        map.insert(USER_ROLE, "filename".into());
        map
    }

}


qrc!(my_resource,
    "imageviewruler" {
        "qml/main.qml",
        "qml/Thumbnails.qml",
    },
);

fn main() -> Result<(), std::io::Error>{
    my_resource();
//    let mut engine = QmlEngine::new();
//    engine.set_object_property("model".into(), &mut imagemodel);
    qml_register_type::<ImageModel>(CStr::from_bytes_with_nul(b"ImageModel\0").unwrap(), 1, 0,
    CStr::from_bytes_with_nul(b"ImageModel\0").unwrap());
    let mut engine = QmlEngine::new();
    engine.load_file("qrc:/imageviewruler/qml/main.qml".into());
    engine.exec();
    Ok(())
}
